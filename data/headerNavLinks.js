const headerNavLinks = [
  { href: '/blog', title: 'Posts' },
  // { href: '/tags', title: 'Tags' },
  { href: '/projects', title: 'Our Work' },
  { href: '/about', title: 'About' },
  { href: '/contact', title: 'Contact' },
  { href: 'https://lancolatech.co.ke', title: 'Home' },
]

export default headerNavLinks
