const projectsData = [
  {
    title: 'Moiben Technical And Vocational College',
    description: `Moiben Technical and Vocational College was established in 2015 
    under the TVET Act 2013 as part of the Kenya Government effort to expand access 
    to TVET and catalyse achievement of the vision 2030.`,
    imgSrc: '/static/images/moibentech.ac.ke_.png',
    href: 'https://moibentech.ac.ke',
  },
  {
    title: 'Mosoriot Technical and Vocational Training Institute',
    description: `The Technical College, whose Mission is to offer 
    world-class training in Technical and Vocational skills, is looking to 
    offer a holistic approach towards Technical courses.
    `,
    imgSrc: '/static/images/mosoriottti.ac.ke_.png',
    href: 'https://mosoriottti.ac.ke',
  },
  {
    title: 'St. Shemus Park Medical Centre',
    description: `St. Shemus Park Medical Centre is a Private Practice – 
    Clinical Officer medical clinic located in Matisi (Kitale Town, Located In Mother’s
       Pharmacy Kitale Building, Top Floor, Next To Kitale Main Bus Park Entrance) 
       Saboti in Trans Nzoia County. As of 2021, the facility was fully operational, 
       regulated by Kenya Medical Practitioners and Dentists Council under registration number 27529.`,
    imgSrc: '/static/images/stshemuspark.co.ke_.png',
    href: 'https://stshemuspark.co.ke',
  },
  {
    title: 'Dr. Diana & Associates Dental Clinic - Eldoret',
    description: `Established in the year 2001 as a single branch in Eldoret, 
    and later another branch in Kitale was born in the year 2018. 
    Through years of experience and deep understanding of the dental 
    profession, they deliver complete, reliable and accessible solutions.
     As their knowledge and expertise grow, so does our range.
    `,
    imgSrc: '/static/images/drdianaandassociatesdentalclinic.co.ke_.png',
    href: 'http://drdianaandassociatesdentalclinic.co.ke/',
  },
  {
    title: 'East African Dental Journal',
    description: `Provider a platform for the publication and 
    dissemination of high-quality research on dental and oral
     health issues specific to the East African region.
    `,
    imgSrc: '/static/images/ea-dentaljournal.com_.png',
    href: 'https://ea-dentaljournal.com/',
  },
  {
    title: 'Links Three Thousand Limited',
    description: `Links Three Thousand Ltd is based in 
    Eldoret County in Kenya. It specializes in Medical Equipment and 
    Medical Supplies.
    `,
    imgSrc: '/static/images/linksthreethousandltd.co.ke_.png',
    href: 'https://linksthreethousandltd.co.ke/',
  },
  {
    title: 'Maxident Maxillofacial And Dental Clinic',
    description: `MaxiDent Company Limited is a duly 
    registered company in Kenya that operates the Maxillofacial
     and Dental Clinic located at The Eldoret Medical 
     Specialists Centre in Eldoret town.
    `,
    imgSrc: '/static/images/maxident.co.ke_.png',
    href: 'http://maxident.co.ke/',
  },
  {
    title: 'Mercy Hands Domiciliary Care',
    description: `Mercy Hands Domiciliary Care offers
     individualized, dedicated, personal and comprehensive 
     homecare service.
    `,
    imgSrc: '/static/images/mercyhands.co.ke_.png',
    href: 'https://mercyhands.co.ke/',
  },
  {
    title: 'Versatile Dental Solutions',
    description: `Versatile Dental Solutions is an innovative dental practice, offering comprehensive and personalized dental care. Our dental practitioners are skilled, experienced and passionate about nurturing healthy and bright smiles.
    `,
    imgSrc: '/static/images/versatiledentalsolutions.co.ke_(Nest Hub Max).png',
    href: 'https://versatiledentalsolutions.co.ke/',
  },
  {
    title: 'Alpha Dental Services',
    description: `Alpha Dental Services is an affordable dental clinic located on the first floor of vision gate building in Kitale town.
    `,
    imgSrc: '/static/images/alphadentalservices.co.ke_(Nest Hub Max).png',
    href: 'https://alphadentalservices.co.ke/',
  },
  {
    title: 'NextCare Dental Studio',
    description: `NextCare Dental Studio has a wide variety of services available, from essential checkups and cleanings to transformative tooth replacement solutions and cosmetic makeovers. Dr. Munyasa and the rest of our team look forward to taking care of your teeth in a way that fits both your personal lifestyle and the outstanding community we're a part of in nairobi.
    `,
    imgSrc: '/static/images/mercyhands.co.ke_.png',
    href: 'https://nextcaredentalstudio.co.ke/',
  },
  {
    title: 'IVC Milimani Mission Hospital',
    description: `Mercy Hands Domiciliary Care offers
     individualized, dedicated, personal and comprehensive 
     homecare service.
    `,
    imgSrc: '/static/images/ivcmilimanimissonhospital.co.ke_(Nest Hub Max).png',
    href: 'https://ivcmilimanimissonhospital.co.ke/',
  },
  {
    title: 'MTRH Pension',
    description: `Moi Teaching and Referral Hospital (MTRH)
     Staff Pension Scheme was established on 1st July 2000
      as a Non-Registered Pension Scheme providing retirement
       benefits to permanent employees of Moi Teaching and 
       Referral Hospital.
    `,
    imgSrc: '/static/images/mtrhsps.co.ke_home.png',
    href: 'https://mtrhsps.co.ke/',
  },
]

export default projectsData
